var assert = require("assert");
var changeTime = require("./solution");

describe("changeTime()", function () {
  it("Should change to military time", function (){
    assert.deepEqual(changeTime("07:05:45PM"), "19:05:45");
    assert.deepEqual(changeTime("09:28:30PM"), "21:28:30");
  });
  it("Should return 00 on midnight and 12 on noon", function (){
    assert.deepEqual(changeTime("12:05:45PM"), "12:05:45");
    assert.deepEqual(changeTime("12:28:30AM"), "00:28:30");
  });

  it("Should be case-insensitive with AM and PM words", function (){
    assert.deepEqual(changeTime("07:05:45pm"), "19:05:45");
    assert.deepEqual(changeTime("09:28:30PM"), "21:28:30");
  });
});