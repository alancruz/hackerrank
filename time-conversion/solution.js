function changeTime(str) {
  var time = str;
  if (time.match(/pm/ig)) {
    time = time.replace(/pm/ig, "").split(":");
      if (time[0] != "12") {
        time[0] = +time[0] + 12;
        time = time.join(":");
        return time;
      } else {
        time = time.join(":");
        return time;
      }
  }
  if (time.match(/am/ig)) {
    time = time.replace(/am/ig, "").split(":");
    if (time[0] == "12") {
      time[0] = "00";
    }
    time = time.join(":");
    return time;
  }
}
module.exports = changeTime;