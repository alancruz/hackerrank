function processData(input) {
  let lines = input.split("\n");
  let [n, k, q] = lines.shift().split(" ").map(x => +x);
  let array = lines.shift().split(" ");
  array = rotateArray(array, k);
  lines.forEach(x => console.log(array[x]));
}

function rotateArray(array, k) {
  k = k % array.length;
  return array.slice(array.length - k).concat(array.slice(0,array.length - k));
}

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});

module.exports = rotateArray;