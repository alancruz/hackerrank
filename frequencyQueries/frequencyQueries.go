package main

import (
  "fmt"
)

// hackerrank's function definition:
// func freqQuery(queries [][]int32) []int32
// for this solution i created queries variable
// which contains test data, in HackerRank
// the freqQuery mehtod receives queries as param

func freqQuery() {
  result := []int32{}
  queries := [][]int32{
    {1, 1},
    {2, 1},
    {3, 1},
  }
  freqs := make(map[int32]int32)
  freqsCount := make(map[int32]int32)
  for _, query := range queries {
    switch query[0] {
    case 1:
      if freqsCount[freqs[query[1]]] >= 1 {
        freqsCount[freqs[query[1]]] = freqsCount[freqs[query[1]]] - 1
      }
      freqs[query[1]] = freqs[query[1]] + 1
      freqsCount[freqs[query[1]]] = freqsCount[freqs[query[1]]] + 1
      break
    case 2:
      if freqs[query[1]] >= 1 {
        if freqsCount[freqs[query[1]]] >= 1 {
          freqsCount[freqs[query[1]]] = freqsCount[freqs[query[1]]] - 1
        }
        freqs[query[1]] = freqs[query[1]] - 1
        freqsCount[freqs[query[1]]] = freqsCount[freqs[query[1]]] + 1
      }
      break
    default:
      if _, ok := freqsCount[query[1]]; ok {
        if freqsCount[query[1]] > 0 {
          result = append(result, 1)
        } else {
          result = append(result, 0)
        }
      } else {
        result = append(result, 0)
      }
      break
    }
  }

  fmt.Println(result)
}


// using if else statement
//func freqQuery() {
//  result := []int32{}
//  queries := [][]int32{
//    {1, 1},
//    {2, 1},
//    {3, 1},
//  }
//  freqs := make(map[int32]int32)
//  freqsCount := make(map[int32]int32)
//
//  for _, query := range queries {
//    if query[0] == 1 {
//      if freqsCount[freqs[query[1]]] >= 1 {
//        freqsCount[freqs[query[1]]] = freqsCount[freqs[query[1]]] - 1
//      }
//      freqs[query[1]] = freqs[query[1]] + 1
//      freqsCount[freqs[query[1]]] = freqsCount[freqs[query[1]]] + 1
//    } else if query[0] == 2 {
//      if freqs[query[1]] >= 1 {
//        if freqsCount[freqs[query[1]]] >= 1 {
//          freqsCount[freqs[query[1]]] = freqsCount[freqs[query[1]]] - 1
//        }
//        freqs[query[1]] = freqs[query[1]] - 1
//        freqsCount[freqs[query[1]]] = freqsCount[freqs[query[1]]] + 1
//      }
//    } else {
//      if _, ok := freqsCount[query[1]]; ok {
//        if freqsCount[query[1]] > 0 {
//          result = append(result, 1)
//        } else {
//          result = append(result, 0)
//        }
//      } else {
//        result = append(result, 0)
//      }
//    }
//  }
//  fmt.Println(result)
//}
