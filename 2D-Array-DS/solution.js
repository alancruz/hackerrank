process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
    input_stdin += data;
});

process.stdin.on('end', function () {
    input_stdin_array = input_stdin.split("\n");
    main();
});

function readLine() {
    return input_stdin_array[input_currentline++];
}

/////////////// ignore above this line ////////////////////

function main() {
  var arr = [];

  var maxSum;
  var n = 0;
  var top, mid, bot;

  for (let i = 0; i < 6; i++) {
    arr[i] = readLine().split(' ');
    arr[i] = arr[i].map(Number);
  }

  for (let i = 0; i <= 3; i ++ ) {
    let x1 = 0, x2 = 2, x3 = 1;
    while (x1 <= 3) {
      top = rowSum(x1, x2, arr[i]);
      mid = arr[i+1][x3];
      bot = rowSum(x1, x2, arr[i+2]);
      let sum = top + mid + bot;

    if (sum > 0) {
      if (!maxSum) {
        maxSum = sum
      } else if (sum > maxSum) {
        maxSum = sum;
      }
    }
    if (sum <= 0) {
      if (maxSum === undefined) {
        maxSum = sum;
      } else if (sum > maxSum) {
        maxSum = sum;
      }
    }

      x1 ++;
      x2 ++;
      x3 ++;
    };
  }

  function rowSum(xi, xf, row) {
    let sum = 0;

    for (let i = xi; i <= xf; i++) {
      sum += row[i];
    }
    return sum;
  }

  console.log(maxSum);

}
