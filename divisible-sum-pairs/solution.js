function main() {
  var [n, k] = readLine().split(' ').map(Number);
  a = readLine().split(' ').map(Number);
  divisibleSumPair(n, k, a);
}

function divisibleSumPair(n, k, a) {
  let count = 0;
  for (let i = 0; i < n - 1; i ++) {
    for (let j = i + 1; j < n; j ++) {
      if ((a[i] + a[j]) % k === 0) {
        count ++;
      }
    }
  }
  return count;
}


process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
    input_stdin += data;
});

process.stdin.on('end', function () {
    input_stdin_array = input_stdin.split("\n");
    main();
});

function readLine() {
    return input_stdin_array[input_currentline++];
}

module.exports = divisibleSumPair;