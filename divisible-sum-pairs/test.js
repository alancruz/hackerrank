var assert = require("assert");
var divisibleSumPair = require("./solution");

describe("divisibleSumPair()", function () {
  it("Should print the number of pairs", function () {
    assert.equal(divisibleSumPair(6, 3, [1, 3, 2, 6, 1, 2]), 5);
  });
});