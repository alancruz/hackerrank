function countWords(s) {
  return s ? 1 + (s.match(/[A-Z]/g) || []).length : 0;
}
module.exports = countWords;
