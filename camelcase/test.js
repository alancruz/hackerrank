var assert = require("assert");
var countWords = require("./solution");

describe("countWords()", function () {
  it("should count words in camel case", function () {
    assert.deepEqual(countWords("saveChangesInTheEditor"), 5);
  });

  it("should return zero when the string is empty", function () {
    assert.deepEqual(countWords(""), 0);
  });

  it("should count words when there is no capital letters", function () {
    assert.deepEqual(countWords("savechangesintheeditor"), 1);
  });
});