var assert = require("assert");
var processNumber = require("./solution");

describe("processNumber()",function () {
  it("should print the biggest palindrome result", function () {
    assert.deepEqual(processNumber("3943", 4, 1), "3993");
    assert.deepEqual(processNumber("777", 3, 0),"777");
  });

  it("should print -1", function () {
    assert.deepEqual(processNumber("0011", 4, 1), "-1");
  });
});