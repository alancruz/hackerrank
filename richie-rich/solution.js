function main() {
  let [n, k] = readLine().split(" ").map(x => +x);
  console.log(processNumber(readLine(), n, k));
}

function processNumber(number, n, k) {
  let changesCount = 0;
  let result = [];
  let changedIndexes = {};
  let halfLength = Math.ceil(n/2);

  for (let i = 0; i < halfLength; i ++) {
    let d0 = number[i];

    if (i < n - i - 1) {
      let d1 = number[n - i - 1];

      if (d0 != d1) {
        if (changesCount >= k) {
          return "-1";
        }
        if (d0 > d1) {
          changedIndexes[n - i - 1] = true;
          d1 = d0;
        } else {
          changedIndexes[i] = true;
          d0 = d1;
        }
        changesCount ++;
      }

      result[n - i - 1] = d1;
    }

    result[i] = d0;
  }

  for (let i = 0; i < halfLength, changesCount < k; i ++) {
    if (i < n - i - 1) {
      if (result[i] != "9") {
        let changesNeeded = changedIndexes[i] || changedIndexes[n - i - 1] ? 1 : 2;

        if (changesCount + changesNeeded <= k) {
          result[i] = "9";
          result[n - i - 1] = "9";
          changesCount += changesNeeded;
        }
      }
    } else {
      result[i] = "9";
    }
  }

  return result.join("");
}
module.exports = processNumber;