function processData(input) {
  let lines = input.split("\n");
  let n = lines.shift();
  let lengths = lines.shift().split(" ").map(x => +x).sort((a,b) => a-b);
  let isTriangle = (a, b, c) => a + b > c && a + c > b && b + c > a;
  let maximum;
  for (let i = 0; i < n - 2; i++) {
    let a = lengths[i];
    for (let j = i + 1; j < n - 1; j++) {
      let b = lengths[j];
      for (let x = j + 1; x < n; x++){
        let c = lengths[x];
        if (isTriangle(a, b, c)) {
          let p = a + b + c;
          if (!maximum || maximum.p < p || maximum.p == p && maximum.a < a) {
            maximum = { a: a, b: b, c: c, p: p };
          }
        }
      }
    }
  }

  console.log(maximum ? `${maximum.a} ${maximum.b} ${maximum.c}` : -1);
}

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});
