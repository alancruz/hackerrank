/*non-degenerate triangle
los triangulos degenerados son aquellos que no cumplen con lo siguiente:
-la suma de los angulos de un triangulo debe ser 180 grados.
-la suma de cualesquiera dos lados deben ser mas grandes que el angulo restante.
-el angulo debe ser positivo.

por lo anterior se dedujo el siguiente codigo para verifiacar que se un triangulo no degenerado.
*/
function isTriangle(a, b, c) {
  return (a + b > c) && (a + c > b) && (b + c > a);
}
