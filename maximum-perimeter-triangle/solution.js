function processData(input) {
  let lines = input.split("\n");
  let [n, stickLength] = lines;
  stickLength = stickLength.split(" ");
  var triangles = [];
  var biggerTrianglesArray = [];
  var sortedBiggerSizeArray = [];
  let a;
  let b;
  let c;
  var biggerPerimeter = 0;
  var longestMaximumSide = 0;
  var biggerSizeElement = 0;
  for (let i = 0; i < stickLength.length - 2; i++) {
     a = +stickLength[i];
    for (let j = i + 1; j < stickLength.length - 1; j++) {
      b = +stickLength[j];
      for (let x = j + 1; x < stickLength.length; x++) {
        c = +stickLength[x];

        if (isTriangle(a, b, c)) {
          createTriangle(a, b, c);
        }
      }
    }
  }
  for (let i = 0; i < triangles.length; i++) {
    if (triangles[i].p > biggerPerimeter) {
      biggerPerimeter = triangles[i].p;
    }
  }

  biggerTriangles(biggerPerimeter);

  if (biggerTrianglesArray.length > 1) {
    for (let i = 0; i < biggerTrianglesArray.length; i++) {
      if (biggerTrianglesArray[i].a > longestMaximumSide) {
        longestMaximumSide = biggerTrianglesArray[i].a
        biggerSizeElement = i;
      }
      if (biggerTrianglesArray[i].b > longestMaximumSide) {
        longestMaximumSide = biggerTrianglesArray[i].b
        biggerSizeElement = i;
      }
      if (biggerTrianglesArray[i].c > longestMaximumSide) {
        longestMaximumSide = biggerTrianglesArray[i].c
        biggerSizeElement = i;
      }
    }
    sortedBiggerSizeArray.push(biggerTrianglesArray[0].a);
    sortedBiggerSizeArray.push(biggerTrianglesArray[0].b);
    sortedBiggerSizeArray.push(biggerTrianglesArray[0].c);
    sortedBiggerSizeArray = sortedSidesArray(sortedBiggerSizeArray);
    console.log(`${sortedBiggerSizeArray[0]} ${sortedBiggerSizeArray[1]} ${sortedBiggerSizeArray[2]}`);
  }

  if (biggerTrianglesArray.length == 1) {
    sortedBiggerSizeArray.push(biggerTrianglesArray[0].a);
    sortedBiggerSizeArray.push(biggerTrianglesArray[0].b);
    sortedBiggerSizeArray.push(biggerTrianglesArray[0].c);
    sortedBiggerSizeArray = sortedSidesArray(sortedBiggerSizeArray);
    console.log(`${sortedBiggerSizeArray[0]} ${sortedBiggerSizeArray[1]} ${sortedBiggerSizeArray[2]}`);
  } else if (biggerTrianglesArray.length == 0) {
    console.log(-1);
  }

  function sortedSidesArray(arr) {
    return sortedBiggerSizeArray.sort(function(a, b){return a-b});
  }

  function biggerTriangles(biggerPerimeter) {
    for (let i = 0; i < triangles.length; i++) {
      if (triangles[i].p == biggerPerimeter) {
        biggerTrianglesArray.push({a:triangles[i].a, b:triangles[i].b, c:triangles[i].c});
      }
    }
  }

  function createTriangle(a, b, c) {
    triangles.push({a:+a, b:+b, c:+c, p:(+a + +b + +c)});
  }

  function isTriangle(a, b, c) {
    return (a + b > c) && (a + c > b) && (b + c > a);
  }
}

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});
