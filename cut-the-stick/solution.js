process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
    input_stdin += data;
});

process.stdin.on('end', function () {
    input_stdin_array = input_stdin.split("\n");
    main();
});

function readLine() {
    return input_stdin_array[input_currentline++];
}

/////////////// ignore above this line ////////////////////

function main() {
  var n = parseInt(readLine());
  arr = readLine().split(' ').map(Number);
  stickCutter(arr);
}

function stickCutter(arr) {
  var arrTest = [];
  while (arr.length) {
    var count = 0;
    let numMenor = Math.min(...arr);
    for (let i = 0; i < arr.length; i ++) {
      arr[i] -= numMenor;
      count ++;
    }
    arrTest.push(count);
    //console.log(count);
    arr = arr.filter(x => x > 0);
  }
  return arrTest;
}

module.exports = stickCutter;