var assert = require("assert");
var stickCutter = require("./solution");

describe("stickCutter()", function () {
  it("should return 6 4 2 1", function () {
    assert.deepEqual(stickCutter([5, 4, 4, 2, 2, 8]), [6, 4, 2, 1]);
  });
});