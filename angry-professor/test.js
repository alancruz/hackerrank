var assert = require("assert");
var determineClass = require("./solution");

describe("determineClass()", function () {
  it("should return YES when the amount of studnes is < k", function () {
    assert.equal(determineClass(3, [1, -3, 4, 2]), "YES");
  });

  it("should return NO when the amount of students is > k", function () {
    assert.equal(determineClass(2,[0, -1, 2, 1]), "NO");
  });
});