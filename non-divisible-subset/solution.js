function processData(input) {
  let lines = input.split("\n");
  let [n, k] = lines.shift().split(" ");
  let s = lines.shift().split(" ");
  let reps = {};
  let count = 0;
  let compareCount = 1;
  //console.log("k: " + k)
  for (let i = 0; i < k; i++) {
      reps[i] = 0;
    }
  s = s.map(function (x) {
    x = x % k;
    if (reps[x] >= 0){
      reps[x] += 1;
    } 
  });//formar objeto con modulos y sus repeticiones
  let len = Object.keys(reps).length;
  //console.log("object Length: "+len);
  //console.log(reps);
  for (x in reps) {
    //console.log("objProperty:"+x);
    if (x != 0){
      if (x !== k/2) {
      if(len > 1){
        if (compareCount <= k/2 ){
          var compareLastProperty = Object.keys(reps)[len - compareCount];//la ultima propiedad del objeto reps
          var compareLastValue = reps[Object.keys(reps)[len - compareCount]]//el valor del ultimo objeto
          compareCount ++;
        } else {
          var compareLastProperty = 0;
        }
        //console.log("LASTproperty: " + compareLastProperty);
        //console.log("LASTpropertyValue: " + compareLastValue);
        if(compareLastProperty > 0) {
          if((+x + +compareLastProperty) != k) {
            count += reps[x];
            count += compareLastValue;
          } else {
            if (reps[x] > compareLastValue) {
              count += reps[x];
              compareLastValue = 0;
            } else if (reps[x] < compareLastValue) {
              count += compareLastValue;
            } else if (reps[x] == compareLastValue) {
              if (reps[x] == 0 && compareLastValue== 0) {
                count += 0;
              } else {
                count += 1;
              }
            }
          }
        }
     } else {
       count += reps[x];
     }
   } else {
     if (reps[x] > 0 ) {
       count += 1;
     }
    }
   } else {
     if (reps[x] > 0 ){
        count += 1;
      }
   }
    //console.log("count until now: "+count);
  }//fin del for in
  console.log(count);
} 
process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});