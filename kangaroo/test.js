var assert = require("assert");
var compareJumps = require("./solution");

describe("compareJumps()", function () {
  it("should return YES", function () {
    assert.deepEqual(compareJumps(`0 3 4 2`), "YES");
  });

  it("should return NO", function () {
    assert.deepEqual(compareJumps(`0 2 5 3`), "NO");
  });
});