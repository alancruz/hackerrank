function compareJumps(str) {
  var [x1, v1, x2, v2] = str.split(' ');
 return v2 >= v1 || (x2 -x1) % (v1 - v2) > 0 ? "NO" : "YES";
}
module.exports = compareJumps;