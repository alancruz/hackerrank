var assert = require("assert");
var processData = require("./solution");

describe("processData()", function () {
  it("Should return the number of changes", function () {
    assert.deepEqual(processData("5\nAAAA\nBBBBB\nABABABAB\nBABABA\nAAABBB"), "3\n4\n0\n0\n4\n");
    assert.deepEqual(processData("1\nAABBC"), "2\n");
  });

  it("Should return zero", function () {
    assert.deepEqual(processData("ABABAB"), 0);
    assert.deepEqual(processData("ABAB"), 0);
  });
});